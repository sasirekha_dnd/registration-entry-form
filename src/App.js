//import logo from './logo.svg';

import { useState } from 'react';
import Email from './Email'
import './App.css';

function App() {

  // const  defaultvalue ={
  //   firstname: ''
  //   lname: ,
  //   email: ,
  //   city: ,
  //   country: 
  //   gender: e
  // }

  // const [formData, setFormData] = useState(defaultvalue)
  const [fname, setFname] = useState('')

  const handleSubmit = (e) =>{
    e.preventDefault();
    console.log(e.target)
    const formValues = {
      firstname: e.target.fname.value,
      lname: e.target.lname.value,
      email: e.target.email.value,
      city: e.target.city.value,
      country: e.target.country.value,
      gender: e.target.gender.value,
    }
    // setFormData([...formData, formValues]);
    // const first = e.target.fname.value;
    // const last = e.target.lname.value;
    // console.log("First name : " +first,"\n","Last name : " +last);
    // const email = e.target.email.value;
    // console.log("Email id : "+email);
    // const city = e.target.city.value;
    // console.log("City : "+city);
    // const country = e.target.country.value;
    // console.log("Country : "+country);
    // const gender = e.target.gender.value;
    // console.log("Gender :"+gender);
  } 

  return (

    <div class="wrapper">
    <div class="registration_form">
      <div class="title">
        Registration
      </div>
      {fname}
      <form onSubmit={handleSubmit} id="form">
        <div class="form_wrap">
          <div class="input_grp">
            <div class="input_wrap">
              <label for="fname">First Name</label>
              <input type="text" name="firstname" id="fname" onChange={(e) => setFname(e.target.value)} placeholder='' ></input>
            </div>
            <div class="input_wrap">
              <label for="lname">Last Name</label>
              <input type="text" name="lastname" id="lname" placeholder='' ></input>
            </div>
          </div>
          <Email />
          <div class="input_wrap">
            <label>Gender</label>
            <ul>
              <li>
                <label class="radio_wrap">
                  <input type="radio" name="gender" value="male" class="input_radio" ></input>
                  <span>Male</span>
                </label>
              </li>
              <li>
                <label class="radio_wrap">
                  <input type="radio" name="gender" value="female" class="input_radio"></input>
                  <span>Female</span>
                </label>
              </li>
            </ul>
          </div>
          <div class="input_wrap">
            <label for="city">City</label>
            <input type="text" id="city" placeholder='' ></input>
          </div>
          <div class="input_wrap">
            <label for="country">Country</label>
            <input type="text" id="country" placeholder='' ></input>
          </div>
          <div class="input_wrap">
            <input type="submit" value="Register Now" class="submit_btn"></input>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  
      );
}



export default App;





